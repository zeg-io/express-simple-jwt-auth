let express = require('express'),
  path = require('path'),
  favicon = require('serve-favicon'),
  logger = require('morgan'),
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser'),
  sassMiddleware = require('node-sass-middleware')

let index = require('./routes/index'),
  users = require('./routes/users')

// TODO: **************************
// TODO: ***** IMPLEMENTATION *****
let ssoApi = require('../lib/middleware')

// TODO: **************************

// db connection
let db = require('./models')
let dockerSecreto = require('docker-secreto')

let app = express()

global.app = app

const mongo = {
  uri: 'mongodb://dev.mongo.website.com:27017/moss?readPreference=primary',
  options: {
    user: 'moss-service',
    pass: dockerSecreto('moss_service_dev', 'BAD-PASSWORD'),
    auth: {
      authdb: 'admin'
    },
    useNewUrlParser: true
  }
}

db.init(mongo.uri, mongo.options)

const UserPermissionsModel = db.mongoose.model('UserPermissions')
const ssoSecret = dockerSecreto('sso_secret', 'BAD-SSO-PASSWORD')
const expressJwtOptions = {}

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs')

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}))
app.use(express.static(path.join(__dirname, 'public')))

// TODO: **************************
// TODO: ***** IMPLEMENTATION *****
app.use(ssoApi.middleware(ssoSecret, UserPermissionsModel, expressJwtOptions, {
  path: [
    '/users'
  ]
}))

// TODO: ---- MAP /auth ROUTE ---- <-- REQUIRED
app.use('/auth', ssoApi.authRoute)

// TODO: **************************

app.use('/', index) // SECURED
app.use('/users', users) // UNSECURED
app.use('/sec', users) // SECURED

// catch 404 and forward to error handler
app.use((req, res, next) => {
  let err = new Error('Not Found')

  err.status = 404
  next(err)
})

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
