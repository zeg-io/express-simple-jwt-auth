let express = require('express')
let router = express.Router()

/* GET users listing. */
router.get('/', (req, res, next) => {
  const { deny } = req.user

  // deny no-one
  return res.send('Sure, you get a response!')
})

router.get('/allow', (req, res, next) => {
  const { allow, profile } = req.user

  allow(['Developers'], () => {
    return res.send('you are a Developer+')
  })
  .otherwise(() => {
    res.send('only Developers are allowed+')
  })
})


router.get('/deny', (req, res, next) => {
  req.user.deny(['Developer'], () => {
    return res.send('you are not a Developer')
  })
  .otherwise(() => {
    res.send('only Developers are rejected')
  })
})

module.exports = router
