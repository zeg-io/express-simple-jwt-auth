// Bring Mongoose into the app

let mongoose = require('mongoose')

// Use default
mongoose.Promise = global.Promise

let isConnected = false,
    mongoURI = ''

// ObjectId Helper
// let ObjectId = mongoose.Types.ObjectId

let initMongo = (dbURI, options) => {
  // Create the database connection
  console.info('Attempting to connect to Mongo DB @ ' + dbURI)

  mongoose.connect(dbURI, options)
  mongoURI = dbURI

  // mongoClient.connect(dbURI, (err, database) => {
  //   if (err) {
  //     throw err
  //   }
  //
  //   module.exports.mongoDb = database
  //   log.good('Mongo DB Native Driver connected.')
  // })
}

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', () => {
  console.info('Mongoose default connection open to ' + mongoURI)
})

// If the connection throws an error
mongoose.connection.on('error', err => {
  console.error('Mongoose default connection error: ' + err)
  isConnected = false
  errorHandler(null, err)
})

// When the connection is disconnected
mongoose.connection.on('disconnected', () => {
  console.error('Mongoose default connection disconnected')
})

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    console.error('Mongoose default connection disconnected through app termination')
    process.exit(0)
  })
})

let middleware = function (req, res, next) {
  // TODO: After good connection and connection drops, error out to client

  if (mongoose.connection.readyState !== 1) {
    console.info('Mongo is not connected, attempting connection...')

    mongoose.connect(mongoURI).then(conn => {
      if (typeof conn !== 'undefined') {
        if (conn.connected) {
          console.info('done.')
          isConnected = true
          next()
        } else {
          isConnected = false
          errorHandler(res, {
            code: 'NOT_CONNECT',
            message: 'Database connection error'
          })
          next()
        }
      }
    }).catch(err => {
      isConnected = false
      errorHandler(res, err)
      next()
    })
  } else next()
}
/* istanbul ignore next */
let errorHandler = (response, RequestError) => {
  let message = '',
      errMsg = RequestError.message,
      code = (/([A-Z])\w+/).exec(errMsg)

  isConnected = false
  switch (code) {
    case 'ECONNREFUSED':
      message = 'Mongo Server unreachable.'
      break
    default:
      message = 'Mongo Server error: ' + code
      break
  }

  console.error('Mongo Server error: [' + code + '] ' + errMsg)
  if (response !== null) {
    response.status(500).json({
      statusCode: 'ERROR',
      message: message
    })
  }
}

module.exports.init = initMongo
module.exports.mongoose = mongoose
module.exports.validateConnectionUp = middleware
module.exports.isConnected = isConnected
module.exports.errorHandler = errorHandler

// BRING IN SCHEMAS & MODELS
require('./users')
