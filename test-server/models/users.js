let mongoose = require('mongoose')
let Schema = mongoose.Schema

const UsersSchema = new Schema(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true, distinct: true },
    salt: { type: String, required: true },
    password: { type: String, required: true },
    employeeId: Number,
    roles: [String],
    hidden: { type: Boolean, default: false }
  },
  { timestamps: { createdAt: 'createdAt' }, collection: 'userPermissions' }
)

mongoose.model('UserPermissions', UsersSchema)
