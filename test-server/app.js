let express = require('express'),
  path = require('path'),
  favicon = require('serve-favicon'),
  logger = require('morgan'),
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser'),
  sassMiddleware = require('node-sass-middleware'),
  cors = require('cors')

const db = require('./models')
const dockerSecreto = require('docker-secreto')

const mongo = {
  // uri: 'mongodb://localhost:27017/testAuth?readPreference=primary',
  uri:
    'mongodb+srv://gm-toolbox@gm-toolbox-cluster-gvxig.mongodb.net/test?retryWrites=true',
  options: {
    user: 'gm-toolbox',
    pass: dockerSecreto('gm-toolbox', 'NO PASSWORD FOUND'),
    auth: {
      authdb: 'admin'
    },
    useNewUrlParser: true
  }
}

db.init(mongo.uri, mongo.options)

let index = require('./routes/index'),
  users = require('./routes/users')

// TODO: **************************
// TODO: ***** IMPLEMENTATION *****
const { simpleAuth, simpleAuthRoutes } = require('../lib/middleware')
const PasswordValidator = require('password-validator')

let passwordSchema = new PasswordValidator()

passwordSchema
  .is()
  .min(8)
  .is()
  .max(100)

const getUserPromise = require('./handlers/getUserPromise')
const createNewAccountPromise = require('./handlers/createNewAccountPromise')
const updateUserPromise = require('./handlers/updateUserPromise')

const jwtSecret = {
  secret: 'My secret passcode',
  algorithm: 'HS512'
}

// TODO: **************************

// db connection
// let db = require('./models')

let app = express()

global.app = app
global.ssoToken = ''

const expressJwtOptions = {}

const simpleJwtAuthOptions = {
  jwtSecret: jwtSecret,
  account: {
    create: createNewAccountPromise,
    get: getUserPromise,
    update: updateUserPromise,
    passwordValidation: passwordSchema
  },
  expressJwtOptions,
  requireAuthUnless: {
    path: ['/users', /\/account.*/, /\/login\/.*/]
  }
}

// TODO: REMOVE THIS
function print(path, layer) {
  if (layer.route) {
    layer.route.stack.forEach(
      print.bind(null, path.concat(split(layer.route.path)))
    )
  } else if (layer.name === 'router' && layer.handle.stack) {
    layer.handle.stack.forEach(
      print.bind(null, path.concat(split(layer.regexp)))
    )
  } else if (layer.method) {
    console.log(
      '%s /%s',
      layer.method.toUpperCase(),
      path
        .concat(split(layer.regexp))
        .filter(Boolean)
        .join('/')
    )
  }
}

function split(thing) {
  if (typeof thing === 'string') {
    return thing.split('/')
  } else if (thing.fast_slash) {
    return ''
  } else {
    var match = thing
      .toString()
      .replace('\\/?', '')
      .replace('(?=\\/|$)', '$')
      .match(/^\/\^((?:\\[.*+?^${}()|[\]\\\/]|[^.*+?^${}()|[\]\\\/])*)\$\//)
    return match
      ? match[1].replace(/\\(.)/g, '$1').split('/')
      : '<complex:' + thing.toString() + '>'
  }
}
// TODO: ABOVE
app.use(cors())

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs')

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(
  sassMiddleware({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    indentedSyntax: true, // true = .sass and false = .scss
    sourceMap: true
  })
)
app.use(express.static(path.join(__dirname, 'public')))

// TODO: ****************************
// TODO: ****** IMPLEMENTATION ******
// app.use()
app.use(simpleAuth(simpleJwtAuthOptions))
app.use('/account', simpleAuthRoutes(simpleJwtAuthOptions))

// TODO: ---- MAP /auth ROUTE ---- <-- REQUIRED
// app.use('/auth', authRoute)
// app.use(
//   '/account',
//   accountRoutes(
//     createNewAccountPromise,
//     getUserPromise,
//     updateUserPromise,
//     passwordSchema
//   )
// )

// TODO: **** END IMPLEMENTATION ****
// TODO: ****************************

app.use('/', index) // SECURED
app.use('/users', users) // UNSECURED
app.use('/sec', users) // SECURED

// TODO: REMOVE THE BELOW 2 lines

console.info('LISTENING ON:')
app._router.stack.forEach(print.bind(null, []))

// catch 404 and forward to error handler
app.use((req, res, next) => {
  let err = new Error('Not Found')

  err.status = 404
  next(err)
})

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
