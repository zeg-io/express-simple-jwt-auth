const db = require('../models')

const getUserPromise = userObject => {
  const userPermissionsMongooseModel = db.mongoose.model('UserPermissions')

  return userPermissionsMongooseModel
    .findOne({ email: userObject.email })
    .then(user => {
      if (!user) return null
      return user.toJSON()
    })
}

module.exports = getUserPromise
