const db = require('../models')

const updateUserPromise = user => {
  const userPermissionsMongooseModel = db.mongoose.model('UserPermissions')

  return userPermissionsMongooseModel
    .findOne({ email: user.email })
    .then(user => {
      if (user) {
        // TODO: UPDATE USER
        return user
      } else {
        return {}
      }
    })
}

module.exports = updateUserPromise
