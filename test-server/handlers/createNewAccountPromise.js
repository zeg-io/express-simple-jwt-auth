const db = require('../models')

const createNewAccountPromise = user => {
  const userPermissionsMongooseModel = db.mongoose.model('UserPermissions')

  return userPermissionsMongooseModel
    .findOne({ email: user.email })
    .then(existingUser => {
      if (existingUser) throw { status: 409, message: 'User already exists' }

      // TODO: Validation here

      return userPermissionsMongooseModel.create(user).then(createdUser => {
        const returnedUser = createdUser.toJSON()

        delete returnedUser.hidden
        return returnedUser
      })
    })
}

module.exports = createNewAccountPromise
