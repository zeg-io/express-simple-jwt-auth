let router = require('express').Router()

/* GET users listing. */
router.get('/', (req, res) => {
  let logMessage = 'Authentication attempt ['

  if (req.user && req.user.email) {
    logMessage += req.user.email
    // has a valid JWT token
    // delete jwt info
    delete req.user.exp
    delete req.user.iat

    if (req.user.roles) {
      logMessage += '] valid profile.'
      res.status(200).json(req.user)
    } else {
      logMessage += '] no valid profile exists.'
      // console.info(`WARN: User (${req.user} <${req.user.email}>) has no profile...`)
      res.status(403).json({ message: 'User has no account' })
    }
  } else {
    logMessage += '] invalid token.'
    res.status(401).json({ message: 'User token not validated' })
  }
  console.log(logMessage)
})

module.exports = router
