const { generateSaltAndHash, hashPasswordWithSalt } = require('./password-help')
const extend = require('util')._extend
const jwt = require('jwt-simple')

const cleanUserObject = user => {
  delete user.salt
  delete user.password

  // In case it's mongo, bake the cleanup in
  delete user.__v
  delete user._id
  delete user.hidden
  delete user.deleted

  return user
}

// IMPORTANT: this function assumes the login is already validated.
const jwtGen = ({ jwtObject, expirationInDays, jwtSecret }) => {
  const currentTime = new Date().getTime() / 1000,
    exipirationTime = expirationInDays * 86400 // 86400 = seconds in a day. 24 * 60 * 60

  jwtObject.exp = currentTime + exipirationTime
  jwtObject.iat = currentTime

  const token = jwt.encode(jwtObject, jwtSecret.secret, jwtSecret.algorithm)

  delete jwtObject.exp
  delete jwtObject.iat

  return {
    user: jwtObject,
    token: token
  }
}

const createAccount = (body, createNewAccountPromise, passwordSchema) => {
  if (!passwordSchema.validate(body.password)) {
    return Promise.reject({
      status: 400,
      message: 'Password did not meet the requirements.'
    })
  }
  const saltHash = generateSaltAndHash(body.password)

  body.salt = saltHash.salt
  body.password = saltHash.hash

  return createNewAccountPromise(body).then(user => {
    return cleanUserObject(user)
  })
}

const validateLogin = (email, pass, getUserPromise, jwtSecret) =>
  getUserPromise(email).then(foundUser => {
    if (
      foundUser &&
      hashPasswordWithSalt(foundUser.salt, pass) === foundUser.password
    ) {
      // user is valid, return JWT Token

      return jwtGen({
        jwtObject: cleanUserObject(foundUser),
        expirationInDays: 30,
        jwtSecret
      })
    }
    throw { status: 401, message: 'Invalid credentials.' }
  })

const updateAccount = (body, requestUser, updateUserPromise) => {
  if (body && body.email && requestUser && requestUser.email) {
    if (body.email === requestUser.email)
      return updateUserPromise(body, requestUser).then(user =>
        cleanUserObject(user)
      )
    return Promise.reject(new Error('Invalid credentials.'))
  }
}

module.exports = {
  create: createAccount,
  login: validateLogin,
  update: updateAccount
}
