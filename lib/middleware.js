const app = require('express')()
const router = require('express').Router()
const jwt = require('jsonwebtoken'),
  expressJwt = require('express-jwt')

const simpleAuthRoutes = require('./simpleAuthRoutes')

const authException = (message, number = -1) => ({
  message,
  name: 'AuthException',
  number
})

const decodeUserToken = (auth, jwtSecret) => {
  let token = auth.replace('Bearer ', '')

  return jwt.verify(token, jwtSecret.secret, jwtSecret.algorithms)
}
const allowRoles = (allowedRoleArray, userRoleArray, callback) => {
  const userRoleLength = userRoleArray.length

  for (let userRoleIndex = 0; userRoleIndex < userRoleLength; userRoleIndex++) {
    if (allowedRoleArray.find(role => role === userRoleArray[userRoleIndex])) {
      callback()
      return true
    }
  }
  return false
}
const denyRoles = (rejectedRoleArray, userRoleArray, callback) => {
  const userRoleLength = userRoleArray.length
  let matchFound = false

  let userRoleIndex = 0

  while (userRoleIndex < userRoleLength && !matchFound) {
    if (rejectedRoleArray.find(role => role === userRoleArray[userRoleIndex]))
      matchFound = true
    userRoleIndex += 1
  }
  if (!matchFound) {
    callback()
    return true
  }
  return false
}
const otherwiseExecute = { otherwise: callback => callback() }
const otherwiseIgnore = { otherwise: callback => {} }

const addProfileToUser = async (
  req,
  res,
  next,
  jwtSecret,
  getUserPromiseFunction,
  blankProfileObject = {}
) => {
  if (req.headers.authorization) {
    try {
      req.user = await decodeUserToken(req.headers.authorization, jwtSecret)
    } catch (err) {
      req.user = {}
      return Promise.reject(authException('Invalid token.', 400))
    }

    // Add role security
    req.user.allow = function(roleArray, callback) {
      if (allowRoles(roleArray, req.user.roles, callback))
        return otherwiseIgnore
      return otherwiseExecute
    }
    req.user.deny = function(roleArray, callback) {
      if (denyRoles(roleArray, req.user.roles, callback)) return otherwiseIgnore
      return otherwiseExecute
    }

    // Add security info to the user object
    return getUserPromiseFunction(req.user).catch(err => {
      console.error('MongoDb User Auth Error', err)
      return Promise.reject(
        authException('Database connection error, see logs', 500)
      )
    })
  } else {
    req.user = {}
    return
  }
}

const authenticator = ({
  account,
  baseRoute,
  jwtSecret,
  expressJwtOptionsObjectWithoutSecret = {},
  requireAuthUnless = {}
}) => {
  let secretForJwt = jwtSecret.secret,
    algorithms = jwtSecret.algorithm
  const expressJwtOptions = Object.assign(
    expressJwtOptionsObjectWithoutSecret,
    { secret: secretForJwt, algorithms: [algorithms] }
  )

  return (req, res, next) => {
    // expressJwt().unless() returns a middleware function
    // Call it, overwriting the next with a custom function which handles errors
    expressJwt(expressJwtOptions).unless(requireAuthUnless)(req, res, err => {
      if (err) return next(err)
      addProfileToUser(req, res, next, expressJwtOptions, account.get)
        .then(() => {
          next()
        })
        .catch(err => {
          next(err)
        })
    })
  }
}

module.exports = {
  simpleAuth: authenticator,
  simpleAuthRoutes,
  allowRoles,
  denyRoles,
  passwordHelper: require('./password-help')
}
