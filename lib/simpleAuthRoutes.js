const router = require('express').Router()
const authRoute = require('./authRoute')
const accountRoutes = require('./accountRoutes')

const getRoutes = simpleJwtAuthOptions => {
  const {
    create,
    get,
    update,
    passwordValidation
  } = simpleJwtAuthOptions.account

  router.use('/auth', authRoute)
  router.use(
    accountRoutes(
      create,
      get,
      update,
      passwordValidation,
      simpleJwtAuthOptions.jwtSecret
    )
  )
  return router
}

module.exports = getRoutes
