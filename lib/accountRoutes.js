let router = require('express').Router()

const { create, login, update } = require('./account')

const routeSetup = (
  saveAccountPromise,
  getUserPromise,
  updateUserPromise,
  passwordSchema,
  jwtSecret
) => {
  // Create
  router.post('/', (req, res) => {
    const body = req.body,
      { email, password } = body

    if (email && password) {
      create(body, saveAccountPromise, passwordSchema)
        .then(newUser => {
          res.status(200).json(newUser)
        })
        .catch(error => {
          const status = error.status || 500

          res.status(status).json({ message: error.message })
        })
    } else res.status(400).json({ message: 'Missing data.' })
  })

  router.post('/login', (req, res) => {
    const body = req.body,
      { email, password } = body

    if (email && password) {
      login({ email }, password, getUserPromise, jwtSecret)
        .then(user => {
          res.status(200).json(user)
        })
        .catch(error => {
          const status = error.status || 500

          res.status(status).json({ message: error.message })
        })
    }
  })

  router.put('/', (req, res) => {
    const body = req.body,
      { email } = body

    if (email && email === req.user.email) {
      // TODO: Authenticate the person before allowing to change stuff.
      update(body, req.user, updateUserPromise)
        .then(user => {
          res.status(200).json(user)
        })
        .catch(error => {
          const status = error.status || 500

          res.status(status).json({ message: error.message })
        })
    }
  })

  return router
}

module.exports = routeSetup
