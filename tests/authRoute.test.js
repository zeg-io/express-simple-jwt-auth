const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../test-server/bin/www')

chai.use(chaiHttp)

const should = chai.should()
const authRoute = require('../lib/authRoute')

const apiURI = 'http://localhost:3100'

const validAuth = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsb2NhbGhvc3QiLCJzdWIiOiJhcmNodG85IiwiYXVkIjoibW9zcy52ZXJpem9uY2xvdWRwbGF0Zm9ybS5jb20iLCJleHAiOjE1MTU2OTk5MDg2MzYsIm1haWwiOiJ0b255LmFyY2hlckB2ZXJpem9ud2lyZWxlc3MuY29tIiwiZW1haWwiOiJ0b255LmFyY2hlckB2ZXJpem9ud2lyZWxlc3MuY29tIiwiZGlzcGxheV9uYW1lIjoiQXJjaGVyLCBUb255IiwiZmlyc3RfbmFtZSI6IlRvbnkiLCJsYXN0X25hbWUiOiJBcmNoZXIiLCJtYW5hZ2VyIjoiQ049VjUzMDczNyxPVT1TTyxPVT1FRE4sT1U9QWNjdHMsREM9dXN3aW4sREM9YWQsREM9dnp3Y29ycCxEQz1jb20iLCJncm91cHMiOltdfQ.MyHPyCSS1OtXw_xwSwJW5QF1hMae1z1jLVdKgIlg-zs'

describe('Tests the Auth Route', () => {
  it('calls the /auth ', (done) => {
    setTimeout(() => {
      console.info('ABOUT TO CALL')
      chai.request(apiURI)
      .get('/auth')
      .end((err, res) => {
        res.status.should.equal(401)
        done()
      })
    }, 1000)
  })
})
describe('Tests the Auth Route after server is running', () => {
  it('calls the root with authorization', done => {
    chai.request(apiURI)
    .get('/auth?1')
    .set('Authorization', validAuth)
    .end((err, res) => {
      res.status.should.equal(200)
      done()
    })
  })
  it('calls /auth with invalid auth', done => {
    chai.request(apiURI)
    .get('/auth?2')
    .set('Authorization', validAuth.substr(0, 30))
    .end((err, res) => {
      res.status.should.equal(401)
      done()
    })
  })
  it('calls /auth with NO auth', done => {
    chai.request(apiURI)
    .get('/auth?3')
    .set('Authorization', '')
    .send()
    .end((err, res) => {
      res.status.should.equal(401)
      done()
    })
  })
})

after(() => {
  process.exit(0)
})

