const chai = require('chai'),
      dockerSecreto = require('docker-secreto')

const should = chai.should()
const ssoApi = require('../lib/middleware')

const validAuth = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsb2NhbGhvc3QiLCJzdWIiOiJhcmNodG85IiwiYXVkIjoibW9zcy52ZXJpem9uY2xvdWRwbGF0Zm9ybS5jb20iLCJleHAiOjE1MTU2OTk5MDg2MzYsIm1haWwiOiJ0b255LmFyY2hlckB2ZXJpem9ud2lyZWxlc3MuY29tIiwiZW1haWwiOiJ0b255LmFyY2hlckB2ZXJpem9ud2lyZWxlc3MuY29tIiwiZGlzcGxheV9uYW1lIjoiQXJjaGVyLCBUb255IiwiZmlyc3RfbmFtZSI6IlRvbnkiLCJsYXN0X25hbWUiOiJBcmNoZXIiLCJtYW5hZ2VyIjoiQ049VjUzMDczNyxPVT1TTyxPVT1FRE4sT1U9QWNjdHMsREM9dXN3aW4sREM9YWQsREM9dnp3Y29ycCxEQz1jb20iLCJncm91cHMiOltdfQ.MyHPyCSS1OtXw_xwSwJW5QF1hMae1z1jLVdKgIlg-zs'
const invalidAuth = 'Bearer eyJ0eXAiOiJ.ASD.asdf'
const ssoSecret = dockerSecreto('sso_secret', 'BADPASS')
const db = require('../models')

const mockReq = {
  headers: {
    authorization: 'xxx'
  },
  user: {
    mail: 'stupid@property.name'
  }
}
const mockRes = {
  code: 0,
  status: function (code) {
    this.code = code
    return this
  },
  json: function (response) {
    return { code: this.code, response }
  }
}

const mockUserPermissionsMongooseModel = {
  model: modelName => ({
    findOne: query =>
      Promise.resolve({
        email: query.email,
        roles: ['tester'],
        locations: [{ city: 'Here', state: 'TX' }]
      })
  })
}

const mongo = {
  // uri: 'mongodb://10.75.177.157:27017/moss?readPreference=primary',
  // uri: 'mongodb://localhost:27017/moss?readPreference=primary',
  uri: 'mongodb://dev.mongo.website.com:27017/moss?readPreference=primary',
  options: {
    user: 'moss-service',
    pass: dockerSecreto('moss_service_dev', 'BAD-PASSWORD'),
    auth: {
      authdb: 'admin'
    },
    useNewUrlParser: true
  }
}
const mockUserPermissionsNotFound = {
  model: modelName => ({
    findOne: query => Promise.resolve({})
  })
}
const mockUserPermissionsError = {
  model: modelName => ({
    findOne: query => Promise.reject({ message: 'forced error' })
  })
}
const mockUserPermissionsErrorOnConnect = {
  connect: uri => Promise.reject({ code: 'ECONNREFUSED', message: 'fake fail' }),
}

let req, res

const setup = (auth, jwtSecret = ssoSecret, dbMongooseFaker = mockUserPermissionsMongooseModel) => {
  req = {}
  res = {}
  req = mockReq
  req.headers.authorization = auth
  req.user = {}

  res = mockRes
  global.app = {
    use: () => {
    }
  }

  const middle = ssoApi.authenticator(jwtSecret, mongo, {}, dbMongooseFaker)

  return new Promise((resolve, reject) => {
    middle(req, res, err => {
      if (err) return reject(err)
      return resolve()
    })
  })
}

// before(() => db.init(mongo.uri, mongo.options).then(() => db.mongoose))

describe('Test Valid Authorization Results', () => {
  it('returns a user when password and profile are correct', () =>
    setup(validAuth)
    .then(middlewareResult => {
      should.not.exist(middlewareResult)

      req.user.email.should.equal('user@email.com')
    })
  )
})
describe('Test Invalid Authorization Results', () => {
  it('fails with a missing JWT', () =>
    setup()
    .then(middlewareResult => {
      should.not.exist(middlewareResult)
    })
    .catch(err => {
      req.user.should.deep.equal({})
      err.message.should.equal('No authorization token was found')
    })
  )
  it('fails with an invalid JWT', () =>
    setup(invalidAuth)
    .then(middlewareResult => {
      should.not.exist(middlewareResult)
    })
    .catch(err => {
      should.exist(err)
      err.name.should.equal('UnauthorizedError')
      err.message.should.equal('invalid token')
      req.user.should.deep.equal({})
    })
  )
  it('fails with an invalid password', () =>
    setup(validAuth, 'BADASS')
    .then(middlewareResult => {
      should.not.exist(middlewareResult)
    })
    .catch(err => {
      should.exist(err)
      err.name.should.equal('UnauthorizedError')
      err.message.should.equal('invalid signature')
    })
  )
})
describe('No Profile User', () => {
  it('fails with a missing profile', () =>
    setup(validAuth, ssoSecret, mockUserPermissionsNotFound)
    .then(middlewareResult => {
      should.not.exist(middlewareResult)
      should.exist(req.user.profile)
      req.user.profile.roles.length.should.equal(0)
      req.user.profile.locations.length.should.equal(0)
    })
  )
})
describe('DB Error', () => {
  it('continues to execute Next() if db error', done => {
    setup(validAuth, ssoSecret, mockUserPermissionsError)

    // Cheap work-around to see result
    setTimeout(() => {
      should.exist(req.user)
      should.not.exist(req.user.profile)
      done()
    }, 10)
  })
  it('does NOT to execute Next() if decode error')

  xit('EXAMPLE TEST', () =>
    setup(validAuth)
    .then(middlewareResult => {
      should.not.exist(middlewareResult)

      console.info('USER:', req.user)
      req.user.mail.should.equal('user@email.com')
    })
    .catch(err => {
      console.info('ERROR IF ANY:', err)
    })
  )
})
describe('Test allow and reject', () => {
  it('tests the allowRoles function', () => {
    let here = 0

    ssoApi.allowRoles(['Role1', 'Role2'], ['Role2'], () => {
      here = 1
      here.should.equal(1)
    })
  })
  it('tests the rejectRoles function with no good match', () => {
    let here = 0

    // The role doesn't match, so it gets to execute
    ssoApi.denyRoles(['Role1', 'Role2'], ['RoleNO-MATCH'], () => {
      here = 1
      here.should.equal(1)
    })
  })
  it('tests the rejectRoles function', () => {
    let here = 0

    ssoApi.denyRoles(['Role1', 'Role2'], ['Role2'], () => {
      here = 1
    })
    here.should.equal(0)
  })
})
