# express-simple-jwt-auth

WORK IN PROGRESS, NOT FULLY TESTED, DO NOT USE.

---
---

Automates validation and decoding of JWT tokens, incorporation of the decoded JWT into `req.user` as an object, and the addition of role based security in an opinionated database, and an `/auth` route to validate the JWT and decode it for the client, and a role based security method for end points.

The authenticator is intended to be used with the [jwt-simple-login](#LINK) component.

----
### New Notes
Uses `password-validator` so configuration for that works for passwords

Ensure your custom methods return the not a mongoose object but a json object... use `.toJSON()` on the result you return.

Authentication requires objects which contain:

```js
{
  ...
  email: '...',
  salt: '...',
  password: '...'
}
```

Login and create endpoint requires fields: `username` and `password` along with any other fields.

Create returns `{ body, saltHash: { salt: '...', hash: '...' } }`

Login returns `{ body }`

----


## Installation
```sh
npm i -S jwt-simple-auth
```

## Requirements
Your Mongo database model _must_ include the following:

```js
{
	"email": String,     // For matching with what's in the '.mail' within the jwt.
	"roles": [ String ], // Array of strings of Role names
	"locations": [       // Array of location objects user is associated with (below)
	  { 
	    "city": String,
	    "state": String
	  }
	] // 
}
```

## Usage

Within the app.js file on your node application, require in the component.
Somewhere you must define the jwtSecret to validate your Authorization token. **_Do not store this in your source code as in the example below_** Use `docker-secreto` or some other secret management method.

NOTE: If you are using `app.use(cors())` you will need to include the route _after_ you add the cors module in.

This module uses `express-jwt` to manage unless operations

_app.js_

```js
const { authenticator, authRoute } = require('sso-api-authenticator')

...
const jwtSecret = 'my-secret-password', // Do NOT do this!
      mongoOptions = {
        uri: 'mongodb://someserver.com:27017/db',
        options: {
          ...
        }
      },
      expressJwtOptions = {}
...
// AFTER database connection and mongoose model are defined...

app.use(authenticator(jwtSecret, mongoOptions, expressJwtOptions, {
  path: [
    '/users',
    '/login',
    /\/login\/.*/
  ]
}))

app.use('/auth', authRoute)
```

After including the above code in your server setup you will gain access to a new object, `req.user`.  This contains all data within the JWT, unencoded as well as relevant roles and locations.

Now, securing a route by role is as simple as adding either `req.user.allow()` or `req.user.deny()`

_route/users.js_

```js
...

router.get('/allow', (req, res, next) => {
  req.user.allow(['Developer'], () => {
    return res.send('The user is a Developer')
  })
  .otherwise(() => {
    res.send('The user does not have the Developer role.')
  })
})


router.get('/deny', (req, res, next) => {
  req.user.deny(['Developer'], () => {
    return res.send('The user does not have the Developer role, so is allowed access.')
  })
  .otherwise(() => {
    res.send('The user is a Developer. None Shall Pass!')
  })
})

...
```

## Parameters
**middleware(jwtSecret, MongooseModelForPermissions, _expressJwtOptions_, _unlessObject_)**

|Param|Required|Definition|
|:----|:----:|:---|
| jwtSecret|Y|The secret to use for validating your JWT token|
| mongoOptions |Y|The database connection object: <br /> { <br /> uri: 'mongodb://server.com:2017, <br /> options: { ... } <br />}.|
| expressJwtOptions |N|A passthrough to give access to the `express-jwt` options. Defaults to `{}`|
|unlessObject|N|An object which is placed into the `express-jwt.unless()` method. This defines which end points _not_ to protect with JWT security ... like `/login` |

See [express-jwt documentation](https://github.com/auth0/express-jwt) for details on the _expressJwtOptions_ and _unless_ object settings

